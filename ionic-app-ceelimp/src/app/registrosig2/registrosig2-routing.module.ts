import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Registrosig2Page } from './registrosig2.page';

const routes: Routes = [
  {
    path: '',
    component: Registrosig2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Registrosig2PageRoutingModule {}
