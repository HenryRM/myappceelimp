import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Registrosig2Page } from './registrosig2.page';

describe('Registrosig2Page', () => {
  let component: Registrosig2Page;
  let fixture: ComponentFixture<Registrosig2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Registrosig2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Registrosig2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
