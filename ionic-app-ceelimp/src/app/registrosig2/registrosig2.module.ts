import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Registrosig2PageRoutingModule } from './registrosig2-routing.module';
import { SignaturePadModule } from 'angular2-signaturepad';

import { Registrosig2Page } from './registrosig2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  
    Registrosig2PageRoutingModule,
    SignaturePadModule,
    ReactiveFormsModule
  ],
  declarations: [Registrosig2Page]
})
export class Registrosig2PageModule {}
