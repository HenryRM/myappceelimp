import { Component, ViewChild, OnInit } from '@angular/core';

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { SignaturePad } from 'angular2-signaturepad';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { from } from 'rxjs';


@Component({
  selector: 'app-registrosig2',
  templateUrl: './registrosig2.page.html',
  styleUrls: ['./registrosig2.page.scss'],
})
export class Registrosig2Page implements OnInit{
  
  pdfObj = null;
  myForm: FormGroup;

  constructor(private fb: FormBuilder){}
  
  ngOnInit(){
    this.myForm = this.fb.group({
      TA: ['', Validators.required],
      ce:  ['', Validators.required],
      RI:  ['', Validators.required],
      fecha: ['', Validators.required],
      hora:  ['', Validators.required],
      te:  ['', Validators.required],
      codigo:  ['', Validators.required],
      bs:  ['', Validators.required],
      o1: '',
      o2: '',
      o3: '',
      o4: '',
      o5: '',
      o6: '',
      o7: '',
      o8: '',
      o9: '',
      o10: '',
      o11: '',
      ob: '',
      pl:  ['', Validators.required],
      tia:  ['', Validators.required],
      riel:  ['', Validators.required],
      rielp:  ['', Validators.required],
      ref:  ['', Validators.required],
      zapa:  ['', Validators.required],
      spo:  ['', Validators.required],
      aseo :  ['', Validators.required],
      peld:  ['', Validators.required],
      ganchos:  ['', Validators.required],
      otros :  ['', Validators.required],
      apta:  ['', Validators.required],
      nam:  ['', Validators.required],
      drawComplete: ['', Validators.required],
      firma :  ['', Validators.required]
    });
  }

  pdfDownload(){
    
    const formValue = this.myForm.value;

    const docDef = {

      pageSize: 'A4',

      pageOrientacion: 'portrait',

      pageMargins: [20, 10, 40, 60],

      content: [
        {
          text: 'CHECK LIST DE INSPECCIÓN DE ESCALERAS\n\n',
          style: 'header', fontSize:18, bold: true
        },

        {text: 'Trabajo o actividad:\n', style: 'header', bold: true},
        {
          ul: [
            {
             text : formValue.TA
            }
            
          ]
        },
        
        {text: 'Centro:\n', style: 'header', bold: true},
      
      {
          ul: [
          {
            text : formValue.RI

          }
          ]
        },

        {text: 'Responsable de la Inspección:\n', style: 'header', bold: true},
      
      {
          ul: [
          {
            text : formValue.ce

          }
          ]
        },

        {text: 'Fecha:\n', style: 'header', bold: true},
      
        {
            ul: [
            {
              text : formValue.fecha
  
            }
            ]
          },

          {text: 'Hora:\n', style: 'header', bold: true},
      
          {
              ul: [
              {
                text : formValue.hora
    
              }
              ]
            },
            {text: 'Tipo de Escalera:\n', style: 'header', bold: true},
      
            {
                ul: [
                {
                  text : formValue.te
      
                }
                ]
              },
             
                {text: 'Codigo de Escalera:\n', style: 'header', bold: true},
      
                {
                    ul: [
                    {
                      text : formValue.codigo
          
                    }
                    ]
                  },
                  {text: 'Base superior o plataforma en buen estado:\n', style: 'header', bold: true},
      
                  {
                      ul: [
                      {
                        text : formValue.bs
            
                      }
                      ]
                    },
                    {text: 'Observaciones:\n', style: 'header', bold: true},
      
                    {
                        ul: [
                        {
                          text : formValue.o1
              
                        }
                        ]
                      },
                      {text: 'Peldaños (antideslizantes, no torcidos y en buen estado):\n', style: 'header', bold: true},
      
                  {
                      ul: [
                      {
                        text : formValue.pl
            
                      }
                      ]
                    },
                    {text: 'Observaciones:\n', style: 'header', bold: true},
      
                    {
                        ul: [
                        {
                          text : formValue.o2
              
                        }
                        ]
                      },
                      {text: 'Tirantes de acero en buen estado:\n', style: 'header', bold: true},
      
                  {
                      ul: [
                      {
                        text : formValue.tia
            
                      }
                      ]
                    },
                    {text: 'Observaciones:\n', style: 'header', bold: true},
      
                    {
                        ul: [
                        {
                          text : formValue.o3
              
                        }
                        ]
                      },
                      {text: 'Riel anterior en buen estado:\n', style: 'header', bold: true},
      
                  {
                      ul: [
                      {
                        text : formValue.riel
            
                      }
                      ]
                    },
                    {text: 'Observaciones:\n', style: 'header', bold: true},
      
                    {
                        ul: [
                        {
                          text : formValue.o4
              
                        }
                        ]
                      },
                      {text: 'Riel posterior en buen estado:\n', style: 'header', bold: true},
      
                      {
                          ul: [
                          {
                            text : formValue.rielp
                
                          }
                          ]
                        },
                        {text: 'Observaciones:\n', style: 'header', bold: true},
          
                        {
                            ul: [
                            {
                              text : formValue.o5
                  
                            }
                            ]
                          },
                          {text: 'Los refuerzos se encuentran en buen estado y no presenta algún daño:\n', style: 'header', bold: true},
      
                          {
                              ul: [
                              {
                                text : formValue.ref
                    
                              }
                              ]
                            },
                            {text: 'Observaciones:\n', style: 'header', bold: true},
              
                            {
                                ul: [
                                {
                                  text : formValue.ob
                      
                                }
                                ]
                              },
                              {text: 'Zapatas antideslizante:\n', style: 'header', bold: true},
      
                          {
                              ul: [
                              {
                                text : formValue.zapa
                    
                              }
                              ]
                            },
                            {text: 'Observaciones:\n', style: 'header', bold: true},
              
                            {
                                ul: [
                                {
                                  text : formValue.o6
                      
                                }
                                ]
                              },
                              {text: 'Sistema de polea y cuerda:\n', style: 'header', bold: true},
      
                              {
                                  ul: [
                                  {
                                    text : formValue.spo
                        
                                  }
                                  ]
                                },
                                {text: 'Observaciones:\n', style: 'header', bold: true},
                  
                                {
                                    ul: [
                                    {
                                      text : formValue.o7
                          
                                    }
                                    ]
                                  },
                                  {text: 'Aseo de escaleras (libre de sustancias deslizantes):\n', style: 'header', bold: true},
      
                                  {
                                      ul: [
                                      {
                                        text : formValue.aseo
                            
                                      }
                                      ]
                                    },
                                    {text: 'Observaciones:\n', style: 'header', bold: true},
                      
                                    {
                                        ul: [
                                        {
                                          text : formValue.o8
                              
                                        }
                                        ]
                                      },

                                      {text: 'Peldaños en forma "D" antideslizante:\n', style: 'header', bold: true},
      
                                  {
                                      ul: [
                                      {
                                        text : formValue.peld
                            
                                      }
                                      ]
                                    },
                                    {text: 'Observaciones:\n', style: 'header', bold: true},
                      
                                    {
                                        ul: [
                                        {
                                          text : formValue.o9
                              
                                        }
                                        ]
                                      },
                                      {text: 'Ganchos trabapeldaños:\n', style: 'header', bold: true},
      
                                      {
                                          ul: [
                                          {
                                            text : formValue.ganchos
                                
                                          }
                                          ]
                                        },
                                        {text: 'Observaciones:\n', style: 'header', bold: true},
                          
                                        {
                                            ul: [
                                            {
                                              text : formValue.o10
                                  
                                            }
                                            ]
                                          },
                                          {text: 'Otros::\n', style: 'header', bold: true},
      
                                          {
                                              ul: [
                                              {
                                                text : formValue.otros
                                    
                                              }
                                              ]
                                            },
                                            {text: 'Observaciones:\n', style: 'header', bold: true},
                              
                                            {
                                                ul: [
                                                {
                                                  text : formValue.o11
                                      
                                                }
                                                ]
                                              },
                                              {text: 'Escalera apta para ser usada:\n', style: 'header', bold: true},
      
                                          {
                                              ul: [
                                              {
                                                text : formValue.apta
                                    
                                              }
                                              ]
                                            },
                                            {text: 'Nombres del Supervisor:\n', style: 'header', bold: true},
                              
                                            {
                                                ul: [
                                                {
                                                  text : formValue.nam
                                      
                                                }
                                                ]
                                              },

                                                {text: 'Nombres del Supervisor:\n', style: 'header', bold: true},
                              
                                            {
                                                ul: [
                                                {
                                                  text : formValue.nam
                                      
                                                }
                                                ]
                                              },
                                             
        
      ]
      
    }
    
       this.pdfObj = pdfMake.createPdf(docDef);

       this.pdfObj.download('demo.pdf');
  }

  
  @ViewChild(SignaturePad) signaturePad: SignaturePad;


  public signatureImage: string;
  
  private signaturePadOptions: Object = { 
    'maxWidth':1,
    'minWidth': 1,
    'canvasWidth': 350,
    'canvasHeight': 300
  };

 
  drawComplete(){
    this.signatureImage = this.signaturePad.toDataURL();
    console.log(this.signatureImage);
  }

  clear(){
    this.signaturePad.clear();
  }
}

