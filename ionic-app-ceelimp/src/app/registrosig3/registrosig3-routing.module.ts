import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Registrosig3Page } from './registrosig3.page';

const routes: Routes = [
  {
    path: '',
    component: Registrosig3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Registrosig3PageRoutingModule {}
