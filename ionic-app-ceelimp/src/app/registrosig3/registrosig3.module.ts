import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Registrosig3PageRoutingModule } from './registrosig3-routing.module';

import { Registrosig3Page } from './registrosig3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Registrosig3PageRoutingModule
  ],
  declarations: [Registrosig3Page]
})
export class Registrosig3PageModule {}
