import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Registrosig3Page } from './registrosig3.page';

describe('Registrosig3Page', () => {
  let component: Registrosig3Page;
  let fixture: ComponentFixture<Registrosig3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Registrosig3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Registrosig3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
