import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
 
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then((m) => m.RegisterPageModule),
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then((m) => m.AdminPageModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'verify-email',
    loadChildren: () => import('./verify-email/verify-email.module').then((m) => m.VerifyEmailPageModule),
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then((m) => m.ForgotPasswordPageModule),
  },
  {
    path: 'mainsig',
    loadChildren: () => import('./mainsig/mainsig.module').then( m => m.MainsigPageModule)
  },
  {
    path: 'registrosig1',
    loadChildren: () => import('./registrosig1/registrosig1.module').then( m => m.Registrosig1PageModule)
  },
  {
    path: 'registrosig2',
    loadChildren: () => import('./registrosig2/registrosig2.module').then( m => m.Registrosig2PageModule)
  },
  {
    path: 'registrosig3',
    loadChildren: () => import('./registrosig3/registrosig3.module').then( m => m.Registrosig3PageModule)
  },
  {
    path: 'registrosig4',
    loadChildren: () => import('./registrosig4/registrosig4.module').then( m => m.Registrosig4PageModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
