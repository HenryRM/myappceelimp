import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Registrosig1PageRoutingModule } from './registrosig1-routing.module';
import { SignaturePadModule } from 'angular2-signaturepad';

import { Registrosig1Page } from './registrosig1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  
    Registrosig1PageRoutingModule,
    SignaturePadModule,
    ReactiveFormsModule
  ],
  declarations: [Registrosig1Page]
})
export class Registrosig1PageModule {}

