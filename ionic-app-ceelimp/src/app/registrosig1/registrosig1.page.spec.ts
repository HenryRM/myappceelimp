import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Registrosig1Page } from './registrosig1.page';

describe('Registrosig1Page', () => {
  let component: Registrosig1Page;
  let fixture: ComponentFixture<Registrosig1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Registrosig1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Registrosig1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
