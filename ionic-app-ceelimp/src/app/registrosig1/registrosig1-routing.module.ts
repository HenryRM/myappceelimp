import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Registrosig1Page } from './registrosig1.page';

const routes: Routes = [
  {
    path: '',
    component: Registrosig1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Registrosig1PageRoutingModule {}
