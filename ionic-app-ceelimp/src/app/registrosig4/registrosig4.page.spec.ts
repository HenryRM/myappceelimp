import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Registrosig4Page } from './registrosig4.page';

describe('Registrosig4Page', () => {
  let component: Registrosig4Page;
  let fixture: ComponentFixture<Registrosig4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Registrosig4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Registrosig4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
