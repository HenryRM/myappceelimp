import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Registrosig4PageRoutingModule } from './registrosig4-routing.module';

import { Registrosig4Page } from './registrosig4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Registrosig4PageRoutingModule
  ],
  declarations: [Registrosig4Page]
})
export class Registrosig4PageModule {}
