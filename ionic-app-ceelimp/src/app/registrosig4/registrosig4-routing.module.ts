import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Registrosig4Page } from './registrosig4.page';

const routes: Routes = [
  {
    path: '',
    component: Registrosig4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Registrosig4PageRoutingModule {}
