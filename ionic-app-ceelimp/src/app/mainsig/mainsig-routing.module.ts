import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainsigPage } from './mainsig.page';

const routes: Routes = [
  {
    path: '',
    component: MainsigPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainsigPageRoutingModule {}
