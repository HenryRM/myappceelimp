import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MainsigPage } from './mainsig.page';

describe('MainsigPage', () => {
  let component: MainsigPage;
  let fixture: ComponentFixture<MainsigPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainsigPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MainsigPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
