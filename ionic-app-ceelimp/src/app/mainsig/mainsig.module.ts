import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MainsigPageRoutingModule } from './mainsig-routing.module';

import { MainsigPage } from './mainsig.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainsigPageRoutingModule
  ],
  declarations: [MainsigPage]
})
export class MainsigPageModule {}
